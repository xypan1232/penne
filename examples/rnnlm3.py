"""
Deep recurrent language model. This is similar to rnnlm1.py, but uses minibatches.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import itertools

hidden_dims = 100
depth = 1

#train = lm.read_data("../data/inferno.txt")
train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.txt")
valid = lm.read_data("../data/ptb.valid.txt")

batch_size = 256
num_parallel = 128

layers = [recurrent.LSTM(hidden_dims, -len(vocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims, hidden_dims))
layers.append(recurrent.Map(make_layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stacked(*layers)

def make_network(batch):
    correct = [map(numberizer.numberize, words) for words in batch]
    input = [[numberizer.numberize("<s>")] + nums[:-1] for nums in correct]
    output = rnn.transduce_batch(input, num_parallel)
    loss = constant(0.)
    for i in xrange(len(output)):
        for j in xrange(len(output[i])):
            o = output[i][j]
            loss -= o[correct[i][j]]
    return loss

trainer = Adagrad(learning_rate=0.1)

for epoch in xrange(1):
    random.shuffle(train)
    train_loss = 0.
    train_size = 0
    for batch in lm.batches(train, batch_size):
        loss = make_network(batch)
        train_loss += trainer.receive(loss)
        train_size += sum(len(words) for words in batch)
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for batch in lm.batches(valid, batch_size):
        loss = make_network(batch)
        valid_loss += trainer.receive(loss)
        valid_size += sum(len(words) for words in batch)
    valid_ppl = numpy.exp(valid_loss/valid_size)
    valid_ppl = 0

    print "epoch=%s train=%s valid=%s" % (epoch, train_ppl, valid_ppl)

